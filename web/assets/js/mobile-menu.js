{


    function handleBurgerClicked(e) {
        
        var $menu = document.querySelector('.mobile-nav-menu');

        if(!e.currentTarget.classList.contains('burger-container--open')) {
            e.currentTarget.classList.add('burger-container--open');
            $menu.classList.add('mobile-nav-menu--open');
        } else {
            e.currentTarget.classList.remove('burger-container--open');
            $menu.classList.remove('mobile-nav-menu--open')
        }

    }

    function init(){
        var $burger = document.querySelector('.mobile-burger');
        $burger.addEventListener('click', handleBurgerClicked);
    }

    init();
}