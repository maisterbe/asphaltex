	mapboxgl.accessToken = 'pk.eyJ1IjoibWFpc3RlcmJlIiwiYSI6ImNrOWIzbmtpNTAwbmYzZXBjNGNsY2NybzIifQ.Ob50NG_jyMBPsqjcl2ZFEg';
	var map = new mapboxgl.Map({
	    container: 'map',
        style: 'mapbox://styles/maisterbe/ck6t2gayf0hjx1ip9jbc2z14m',
        center: [3.18650, 50.85400],
        zoom: 15
	});

	map.on('load', function () {
	    map.loadImage(
	        '/assets/img/asphaltex-icon-x.png',
	        function (error, image) {
	            if (error) throw error;
	            map.addImage('x', image);
	            map.addSource('point', {
	                'type': 'geojson',
	                'data': {
	                    'type': 'FeatureCollection',
	                    'features': [{
	                        'type': 'Feature',
	                        'geometry': {
	                            'type': 'Point',
	                            'coordinates': [3.18650, 50.85400]
	                        }
	                    }]
	                }
	            });
	            map.addLayer({
	                'id': 'points',
	                'type': 'symbol',
	                'source': 'point',
	                'layout': {
	                    'icon-image': 'x',
	                    'icon-size': 0.25
	                }
	            });
	        }
	    );
	});