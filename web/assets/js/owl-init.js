{

    function init() {

        $(document).ready(function(){
            $(".home-realisaties-slider").owlCarousel({
                items: 1,
                loop: true,
                autoplayHoverPause: true,
                nav: true,
                navText: ["<img src='/assets/img/slider-arrow-left.svg' />", "<img src='/assets/img/slider-arrow-right.svg' />"],
                margin: 0
            });
        });

    }


    init();
}