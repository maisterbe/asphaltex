{

    var closing = false;
    var accordeonShown = false;

    function closeEverything() {
        
        closing = true;

        var $accordeon = document.querySelector('.accordeon');
        var $checks = $accordeon.querySelectorAll('input');
        var $labels = $accordeon.querySelectorAll('label');

        for(var i = 0; i < $checks.length; i++) {
            $checks[i].checked = false;
        }

    }

    function removeChecks() {
        var $checks = document.querySelectorAll('input');

        for(var i = 0; i < $checks.length; i++) {
            $checks[i].parentElement.classList.remove('active');
            $checks[i].checked = false;
            $checks[i].parentElement.querySelector('.tab-content').style.maxHeight = 0;
        }
    }

    function handleLabelClicked(e) {
        if(e.currentTarget.parentElement.querySelector('input').checked) {
            closeEverything();
        }
    }

    function handleRadioChanged(e) {
        removeChecks();
        if(!closing) {

        e.currentTarget.parentElement.classList.add('active');
        e.currentTarget.parentElement.querySelector('input').checked = true;
        e.currentTarget.parentElement.querySelector('.tab-content').style.maxHeight =  e.currentTarget.parentElement.querySelector('.tab-content').scrollHeight + 'px';

        }

        closing = false;
    } 

    function openFirst($acc) {

        $acc.querySelector('input').checked = true;
        var $tab = $acc.querySelector('.tab');
        $tab.classList.add('active');
        $tab.querySelector('.tab-content').style.maxHeight =  $tab.querySelector('.tab-content').scrollHeight + 'px';
        console.log('this');

    }

    function handleWindowResize() {
        var $accordeon = document.querySelector('.accordeon');

        if(window.innerWidth <= 900 && !accordeonShown) {
            openFirst($accordeon);
        }

        if(window.innerWidth > 900 && accordeonShown) {
            removeChecks();
        }
    }

    function init() {

        var $accordeon = document.querySelector('.accordeon');
        var $checks = $accordeon.querySelectorAll('input');
        var $labels = $accordeon.querySelectorAll('label');

        for(var i = 0; i < $checks.length; i++) {
            $checks[i].addEventListener('change', handleRadioChanged);
        }

        for(var i = 0; i < $labels.length; i++) {
            $labels[i].addEventListener('click', handleLabelClicked);
        }

        if(window.innerWidth <= 900) {
            openFirst($accordeon);
        } else {
            window.addEventListener('resize', handleWindowResize);
        }


    }

    init();

}   