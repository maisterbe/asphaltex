{

    var titles = [];

    function getTitles() {

        var $elems = document.querySelectorAll('.swiper-slide');
        
        for (var i = 0; i < $elems.length; i++) {
            if(!$elems[i].classList.contains('swiper-slide-duplicate')) {
                titles.push($elems[i].getAttribute('data-title'));
            }
        }

    }

    function initDienstenSwiper() {
        var mySwiper = new Swiper ('.diensten-swiper-container', {
            // Optional parameters
            direction: 'vertical',
            loop: true,
        
            // If we need pagination
            pagination: {
              el: '.swiper-pagination',
              clickable: true,
                renderBullet: function (index, className) {
                    console.log(titles[index]);
                return '<span class="' + className + '"><span class="nr">' + (index + 1) + '</span>' + titles[index] + '</span>';
                }
            },
        
            // Navigation arrows
            navigation: {
              nextEl: '.swiper-button-next',
              prevEl: '.swiper-button-prev',
            },
        
            // And if we need scrollbar
            scrollbar: {
              el: '.swiper-scrollbar',
            },
          })
    }

    function initImageThumbsSlider() {

        var galleryThumbs = new Swiper('.realisatie-thumbs-container', {
            slidesPerView: 3,
            spaceBetween: 40,
            freeMode: true,
            watchSlidesVisibility: true,
            watchSlidesProgress: true,
          });

        var mySwiper = new Swiper ('.realisatie-swiper-container', {
            // Optional parameters
            direction: 'horizontal',
            loop: true,
        
            // If we need pagination
            pagination: {
              el: '.swiper-pagination',
              clickable: true,
                renderBullet: function (index, className) {
                    console.log(titles[index]);
                return '<span class="' + className + '"><span class="nr">' + (index + 1) + '</span>' + titles[index] + '</span>';
                }
            },
        
            // Navigation arrows
            navigation: {
              nextEl: '.swiper-button-next',
              prevEl: '.swiper-button-prev',
            },
        
            // And if we need scrollbar
            scrollbar: {
              el: '.swiper-scrollbar',
            },

            thumbs: {
                swiper: galleryThumbs
              }
          });

          
    }

    

    function init() {
        getTitles();
        initDienstenSwiper();
        initImageThumbsSlider();
    }

    init();
}