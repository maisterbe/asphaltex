{

    function parseURLParams(url) {
    var queryStart = url.indexOf("?") + 1,
    queryEnd   = url.indexOf("#") + 1 || url.length + 1,
    query = url.slice(queryStart, queryEnd - 1),
    pairs = query.replace(/\+/g, " ").split("&"),
    parms = {}, i, n, v, nv;
    if (query === url || query === "") return;
    for (i = 0; i < pairs.length; i++) {
    nv = pairs[i].split("=", 2);
    n = decodeURIComponent(nv[0]);
    v = decodeURIComponent(nv[1]);
    if (!parms.hasOwnProperty(n)) parms[n] = [];
    parms[n].push(nv.length === 2 ? v : null);
    }
    return parms;
    }

    function showThanksMessage() {

        var $form = document.querySelector('form');
        var $container = document.querySelector('.form-wrap')

        var $div = document.createElement('div');
        $div.classList.add('thankyou-message');

        var $p = document.createElement('p');
        $p.innerText = 'Bedankt voor uw bericht';

        $div.appendChild($p)

        $container.insertBefore($div, $form);

        setTimeout(function(){
            $div.remove();
        }, 3500);

    }

    function checkFormSent() {
        var answ = parseURLParams(window.location.href);

        if(answ['msg'] == "sent") {
            showThanksMessage();
        }
    }


    function init() {
        checkFormSent();
    }

    init();

}