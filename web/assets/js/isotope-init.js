{

    var currentTags = [];
    var iso;

    function getFilterString() {

        var str = '';

        for(var i = 0; i < currentTags.length; i++) {

            str += '.' + currentTags[i] + ',';
        }
        str = str.substr(0, str.length - 1);
        return str;

    }

    function handleTagClick(e) {

        if (!e.currentTarget.classList.contains('tag-active')) {
            e.currentTarget.classList.add('tag-active');

            if(!currentTags.includes(e.currentTarget.id)) {
                currentTags.push(e.currentTarget.id);
            }

        } else {
            e.currentTarget.classList.remove('tag-active');

            var index = currentTags.indexOf(e.currentTarget.id);
            currentTags.splice(index, 1);
        }

        iso.arrange({ filter: getFilterString() });

    }

    function setUpFilter() {
        var $tags = document.querySelectorAll('.tag');

        for(var i = 0; i < $tags.length; i++) {
            $tags[i].addEventListener('click', handleTagClick)
        }
    }


    function setUpIsotope() {

        var $grid = document.querySelector('.realisaties-grid');
        iso = new Isotope( $grid, {
        // options
        itemSelector: '.realisatie',
        layoutMode: 'fitRows'
        });

    }

    function init() {
        setUpIsotope();
        setUpFilter();
    }

    init();

}